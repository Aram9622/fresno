$(()=>{
    // click on button and change status with ajax
    $('.change-status').on('click', function (){
        const btn = $(this)
        const ID = btn.data('id');
        const field = btn.data('field');
        const ACTIVE = 'Active';
        const INACTIVE = "Inactive";
        const SUCCESS = "btn-success";
        const WARNING = "btn-warning";
        let status = 0;
        if(btn.hasClass(SUCCESS)){
            btn.html(INACTIVE);
            btn.removeClass(SUCCESS);
            btn.addClass(WARNING);
            status = 0;
        }else {
            btn.html(ACTIVE);
            btn.removeClass(WARNING);
            btn.addClass(SUCCESS);
            status = 1;
        }

        $.post({
            url: `/admin/change-status-action/${ID}`,
            data: {status,field},
            success: function (res){
                console.log(res);
            }
        })
    })
    
})
