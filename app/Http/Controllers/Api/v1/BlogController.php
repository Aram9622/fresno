<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service\BlogService;
use App\Helper\Helper;
use App\Http\Requests\BlogCommentsRequest;

class BlogController extends Controller
{
    public $blog;
    public function __construct(BlogService $blog){
        $this->blog = $blog;
    }

    public function getBlogs($slug = null){
        $blog = $slug ? $this->blog->getById($slug) : $this->blog->get();
        return Helper::responseHandler("get blog message", 200, true, $blog);
    }

    public function getBlogsByCategory($slug){
        return Helper::responseHandler("get blog message", 200, true, $this->blog->getByCategory($slug));
    }

    public function createCommentsBlog(BlogCommentsRequest $request){
        return Helper::responseHandler($this->blog->createComments($request), 200, true, []);
    }
}
