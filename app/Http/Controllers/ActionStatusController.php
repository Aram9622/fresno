<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Repositories\MailRepository;
use Illuminate\Http\Request;

class ActionStatusController extends Controller
{
    public function changeActionStatus($id, Request $request){
        Post::query()->where('id', $id)->update([$request->field => $request->status]);
        return response()->json("Success");
    }
}
