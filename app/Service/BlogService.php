<?php

namespace App\Service;

use App\Models\Post;
use Illuminate\Support\Facades\DB;

class BlogService
{
    public $model;

    public function __construct(Post $post)
    {
        $this->model = $post;
    }

    public function get()
    {
        return $this->model->query()->with('category')->get();
    }

    public function getById($slug)
    {
        return $this->model->query()->with(['category', 'comment' => function($query){
            $query->where('status', 1);
        }])->where('slug', $slug)->first();
    }

    public function getByCategory($slug)
    {
        return array_values($this->model->query()->with(['category'])
            ->get()
            ->filter(function ($item) use ($slug){
                return $item->category->slug == $slug;
            })
            ->toArray());
    }

    public function createComments($request){
        try {
            $post = $this->model->query()->findOrFail($request->id);
            $comment = $post->comment()->create($request->validated());

            $postComment = Post::find($request->id);
            $postComment->post_comment()->attach($comment->id);

            return "Comment successfully sent";
        }catch (\Exception $exception){
            return $exception->getMessage();
        }

    }
}
